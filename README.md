# Setup

`yarn`
`yarn start`



# Andy's Coffee Shop

Andy has a coffee shop. Instead of using an off-the-shelf POS system like a normal person, he decided to 
write his own. It works but it often becomes unresponsive, getting stuck on the Loading screen. Andy's staff 
don't know what is wrong and customers are getting annoyed!

Also, if someone tries to order salt, the API literally catches on fire and the POS becomes inoperable 
for the rest of the day, so the shop needs to shut. It's weird.

Please help Andy out by improving the code a bit with some error handling. If the error from the API is 
recoverable or fixable, tell the user what is going on and let them fix it. If the error is unrecoverable, 
tell the user and exit the app gracefully.