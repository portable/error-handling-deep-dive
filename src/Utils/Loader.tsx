import React from 'react';

const Loader = () => (
  <div className="loader-overlay">
    <h1>Loading, please wait</h1>
  </div>
);

export default Loader;
