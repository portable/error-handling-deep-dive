
export class DataError extends Error {
  constructor(message) {
    super(message);
    this.response = {
      status: 400,
      body: { type: 'DataError', error: message },
    };
    this.name = 'DataError';
  }
}


export class OutOfStockError extends Error {
  constructor(message) {
    super(message);
    this.response = {
      status: 400,
      body: { type: 'OutOfStockError', error: message },
    };
    this.name = 'OutOfStockError';
  }
}
