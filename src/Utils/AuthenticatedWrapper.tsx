import React, { useContext } from 'react';
import {
  useHistory,
  useLocation
} from 'react-router-dom';

import { Context, ContextType } from '../Context';

const AuthenticatedWrapper: React.FC = ({ children }) => {
  const { isAuthenticated } = useContext(Context) as ContextType;

  let history = useHistory();
  let location = useLocation();

  if (!isAuthenticated && location.pathname !== '/login') {
    history.push('/login');
    return null;
  }

  return (
    <>
      {children}
    </>
  );
}

export default AuthenticatedWrapper;
