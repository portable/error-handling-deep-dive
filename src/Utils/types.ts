export enum IngredientsEnum {
  COFFEE = 'coffee',
  MILK = 'milk',
  SUGAR = 'sugar',
  TEA = 'tea',
  SALT = 'salt',
};

export type IngredientsType = {
  [key in IngredientsEnum]: number;
};

export type OrderIngredientsType = {
  [key in IngredientsEnum]: boolean;
};

export type Order = {
  date: string,
  name: string,
  description: string,
  ingredients: IngredientsType,
  price: number,
};