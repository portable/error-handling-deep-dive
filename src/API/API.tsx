/**
 * This is a third party API.
 * 
 * Andy will get in big trouble if you modify anything in this file. Please keep Andy out of trouble and leave this file alone.
 */

import { DataError, OutOfStockError } from "../Utils/errors";
import { IngredientsEnum, IngredientsType, Order } from "../Utils/types";

type ResponseType = {
  status: number,
  body: any,
};

let FUCK_SHIT_SOMEONE_ORDERED_SALT = false;


// if our API has thrown an error, return it as an HTTP response
const handleErrors = (error: any) => {
  if ((error as any).response) {
    return (error as any).response;
  } else {
    return Promise.resolve({
      body: { type: 'Error', error: (error as any).message },
      status: 500,
    });
  }
};


export const login = (username: string, password: string) => {
  return new Promise<ResponseType>((resolve, reject) => {
    if (FUCK_SHIT_SOMEONE_ORDERED_SALT) {
      reject(new Error('Sorry, the API is currently on fire.'));
    }

    console.log(username, password);
    
    setTimeout(() => {
      if (username === 'valid' && password === 'user') {
        resolve({
          status: 200,
          body: {
            isValid: true,
          },
        });
      } else if (username === 'error' && password === 'user') {
        reject(new Error('Something went wrong when logging in'));
      } else {
        resolve({
          status: 200,
          body: {
            isValid: false,
          },
        });
      }
    }, 1000)
  })
    .catch(handleErrors);
}



const stockLevels: IngredientsType = {
  coffee: 4,
  milk: 6,
  sugar: 3,
  tea: 2,
  salt: 3,
};

export const getStockLevels = () => {
  return new Promise<ResponseType>((resolve, reject) => {
    if (FUCK_SHIT_SOMEONE_ORDERED_SALT) {
      reject(new Error('Sorry, the API is currently on fire.'));
    }
    
    setTimeout(() => {
      resolve({
        status: 200,
        body: {
          ...stockLevels,
        },
      });

    }, 1000);
  })
    .catch(handleErrors);
};


const orders: Order[] = [];

export const getOrderLog = () => {
  return new Promise<ResponseType>((resolve, reject) => {
    if (FUCK_SHIT_SOMEONE_ORDERED_SALT) {
      reject(new Error('Sorry, the API is currently on fire.'));
    }

    setTimeout(() => {
      resolve({
        status: 200,
        body: {
          isValid: false,
        },
      });
    }, 1000);
  })
    .catch(handleErrors);;
}



export const placeOrder = (order: Order) => {
  return new Promise<ResponseType>((resolve, reject) => {
    setTimeout(() => {
      if (FUCK_SHIT_SOMEONE_ORDERED_SALT) {
        reject(new Error('Sorry, the API is currently on fire.'));
      }

      const { name, ingredients } = order;

      if (ingredients.salt) {
        FUCK_SHIT_SOMEONE_ORDERED_SALT = true;
      }

      if (!name || name === '') {
        reject(new DataError('Name not provided'));
      }

      Object.entries(ingredients)
        .forEach(([ingredient, amount]) => {
          stockLevels[ingredient as IngredientsEnum] -= amount;

          if (stockLevels[ingredient as IngredientsEnum] < 0) {
            reject(new OutOfStockError(`Not enough ${ingredient}`));
          }
        });

      orders.push(order);


      resolve({
        status: 200,
        body: {
          orderLog: orders,
          stockLevels,
        },
      });
    }, 1000)
  })
    .catch(handleErrors);
};
