import './App.css';
import { ContextProvider } from './Context';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import Login from './Components/Login';
import AuthenticatedWrapper from './Utils/AuthenticatedWrapper';
import POS from './Components/POS';

function App() {
  return (
    <div className="App">
      <ContextProvider>
        <BrowserRouter>
          <AuthenticatedWrapper>

            <Switch>
              <Route path="/login">
                <Login />
              </Route>
              <Route path="/pos">
                <POS />
              </Route>
              <Redirect to="/pos" />
            </Switch>
          </AuthenticatedWrapper>

        </BrowserRouter>
      </ContextProvider>
    </div>
  );
}

export default App;
