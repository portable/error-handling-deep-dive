import React, { createContext, useState } from 'react';

export type ContextType = {
  isAuthenticated: boolean;
  setIsAuthenticated: (isAuthed: boolean) => void;
}

export const Context = createContext<ContextType | null>(null);

export const ContextProvider: React.FC = ({
  children,
}) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  return (
    <Context.Provider
      value={{
        isAuthenticated,
        setIsAuthenticated,
      }}
    >
      {children}
    </Context.Provider>
  );
};
