import React, { useEffect, useState } from 'react';
import { getStockLevels, placeOrder } from '../API/API';
import { IngredientsType, Order, IngredientsEnum } from '../Utils/types';
import Loader from '../Utils/Loader';

const prices: IngredientsType = {
  coffee: 3.00,
  tea: 2.40,
  milk: 1.20,
  sugar: 0.50,
  salt: 1.80,
}

const POS = () => {
  const [stockLevels, setStockLevels] = useState<IngredientsType>({ coffee: 0, milk: 0, sugar: 0, tea: 0, salt: 0 });
  const [orderIngredients, setOrderIngredients] = useState<IngredientsType>({ coffee: 0, milk: 0, sugar: 0, tea: 0, salt: 0 });
  const [customerName, setCustomerName] = useState('');
  const [orderLog, setOrderLog] = useState<Order[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    getStockLevels()
      .then(({ body }) => {
        setStockLevels(body);
        setLoading(false);
      })
  }, []);

  const toggleOrderIngredient = (ingredient: string, amount: number) => {
    setOrderIngredients({
      ...orderIngredients,
      [ingredient]: amount,
    })
  }

  const handlePlaceOrder = () => {
    const ingredientsString = Object.entries(orderIngredients)
      .filter(([key, value]) => value > 0)
      .map(([key, value]) => `${key} (${value})`)
      .join(', ');

    const price = Object.entries(orderIngredients)
      .reduce<number>((acc, [ingredient, amount]) => {
        return acc += (prices[ingredient as IngredientsEnum] * amount);
      }, 0);

    setLoading(true);
      
    placeOrder({
      date: new Date().toISOString(),
      name: customerName,
      ingredients: orderIngredients,
      description: `Order for "${customerName}" with ${ingredientsString}`,
      price,
    })
      .then(response => {
        console.log('placeOrder response', response);

        const { orderLog, stockLevels } = response.body;
        setOrderLog([...orderLog]);
        setStockLevels(stockLevels);

        setOrderIngredients({ coffee: 0, milk: 0, sugar: 0, tea: 0, salt: 0 });
        setCustomerName('');

        setLoading(false);
      });
  }

  const { coffee, milk, sugar, tea, salt } = stockLevels;

  return (
    <>
      <h1>POS <span style={{ fontSize: '10px' }}>for</span> Andy's Coffee Shop</h1>

      <h3>Stock levels</h3>

      Coffee: {coffee}<br />
      Milk: {milk}<br />
      Sugar: {sugar}<br />
      Tea: {tea}<br />
      Salt: {salt}<br />

      <hr />

      <h3>Place a new order</h3>

      <p>Enter the amount of each ingredient and the customer name.</p>

      <label>Coffee: <input type="number" onChange={(e) => toggleOrderIngredient('coffee', Number(e.target.value))} value={orderIngredients.coffee} /></label><br />
      <label>Tea: <input type="number" onChange={(e) => toggleOrderIngredient('tea', Number(e.target.value))} value={orderIngredients.tea} /></label><br />
      <label>Milk: <input type="number" onChange={(e) => toggleOrderIngredient('milk', Number(e.target.value))} value={orderIngredients.milk} /></label><br />
      <label>Sugar: <input type="number" onChange={(e) => toggleOrderIngredient('sugar', Number(e.target.value))} value={orderIngredients.sugar} /></label><br />
      <label>Salt: <input type="number" onChange={(e) => toggleOrderIngredient('salt', Number(e.target.value))} value={orderIngredients.salt} /></label><br />
      <br />
      Customer name: <input type="text" onChange={(e) => setCustomerName(e.target.value)} value={customerName} />
      <br />
      <input type="submit" onClick={handlePlaceOrder} />
      <br />

      <hr />

      <h3>Order log</h3>

      {orderLog.map(({ date, description, price }) => (
        <div key={date}>{date} - {description} - ${price}<br /></div>
      ))}

      {loading && <Loader />}
    </>
  );
};

export default POS;