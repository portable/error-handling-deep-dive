import React, { useContext, useEffect, useState } from 'react';
import { Context, ContextType } from '../Context';
import {
  useHistory,
} from 'react-router-dom';
import Loader from '../Utils/Loader';
import { login } from '../API/API';

type FormDataType = {
  username: string,
  password: string,
};

const Login = () => {
  const { isAuthenticated, setIsAuthenticated } = useContext(Context) as ContextType;
  const [isLoginInvalid, setIsLoginInvalid] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [formData, setFormData] = useState<FormDataType>({ username: '', password: '' });

  const history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      history.push('/pos');
    }
    }, [history, isAuthenticated]);

  const handleAuthentication = ({ username, password }: FormDataType) => {
    setIsLoading(true);
    setIsLoginInvalid(false);
    return login(username, password)
      .then(({ status, body: { isValid } }) => {
        if (status === 200) {
          if (isValid) {
            setIsAuthenticated(true);
          } else {
            setIsAuthenticated(false);
            setIsLoginInvalid(true);
          }

          setIsLoading(false);
        } else {
          setIsLoginInvalid(true);
        }
      });
  }

  const updateFormData = (name: string, value: string) => {
    setFormData({
      ...formData,
      [name]: value,
    });
  }

  return (
    <>
      <h1>Login</h1>

      {isLoading && <Loader />}
      {isLoginInvalid && <p><strong>Sorry, your details are incorrect. Please try again.</strong></p>}

      <br />

      <p>Valid user is "valid" / "user"</p>
      <p>For some reason, our new intern's login doesn't seem to work - "error" / "user"</p>

      <form>
        Username: <input type="text" name="username" value={formData.username} onChange={e => updateFormData('username', e.target.value)} />
        Password: <input type="password" name="password" value={formData.password} onChange={e => updateFormData('password', e.target.value)} />
        <br />
        <input type="button" value="Login" onClick={() => handleAuthentication(formData)} />
      </form>
    </>
  );
}

export default Login;
